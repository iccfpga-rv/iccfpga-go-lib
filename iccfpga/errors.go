package iccfpga

const (
	ERROR                  int = 0
	SE_NOT_INITIALIZED     int = -1
	INVALID_SLOT           int = -2
	INVALID_KEY_INDEX      int = -3
	INVALID_SECLEVEL       int = -4
	ERROR_LOADING_KEY      int = -5
	INVALID_KEY            int = -6
	ERROR_START_SE         int = -7
	ERROR_SET_RANDOM_KEY   int = -8
	INVALID_BUNDLE_HASH    int = -9
	INVALID_AUTH           int = -10
	INSECURE_BUNDLE_HASH   int = -11
	INVALID_TRYTES         int = -12
	INVALID_BRANCH         int = -13
	INVALID_TRUNK          int = -14
	INVALID_MWM            int = -15
	TOO_MANY_TX            int = -16
	INVALID_ADDRESS        int = -17
	INVALID_TAG            int = -18
	DATA_TOO_LONG          int = -19
	ERROR_READING_FLASH    int = -20
	INVALID_FLASH_PAGE     int = -21
	INVALID_BASE64_DATA    int = -22
	ERROR_WRITING_FLASH    int = -23
	INVALID_JSON           int = -24
	TOO_FEW_TX             int = -25
	TOO_MANY_ADDRESSES     int = -26
	INVALID_COUNT          int = -27
	NONE                   int = 1
	SE_ALREADY_INITIALIZED int = 2
)

func ErrorMsg(err int) string {
	switch err {
	case NONE:
		return "ok"
	case SE_NOT_INITIALIZED:
		return "secure element not initialized"
	case INVALID_SLOT:
		return "invalid slot"
	case INVALID_KEY_INDEX:
		return "invalid key index"
	case INVALID_SECLEVEL:
		return "invalid secure level"
	case ERROR_LOADING_KEY:
		return "error loading key"
	case INVALID_KEY:
		return "invalid key"
	case ERROR_START_SE:
		return "error starting secure element"
	case ERROR_SET_RANDOM_KEY:
		return "error setting random key"
	case INVALID_BUNDLE_HASH:
		return "invalid bundlehash"
	case INVALID_AUTH:
		return "auth invalid"
	case INSECURE_BUNDLE_HASH:
		return "insecure bundlehash"
	case INVALID_TRYTES:
		return "invalid trytes"
	case INVALID_BRANCH:
		return "invalid branch"
	case INVALID_TRUNK:
		return "invalid trunk"
	case INVALID_MWM:
		return "invalid mwm"
	case TOO_MANY_TX:
		return "too many transactions"
	case INVALID_ADDRESS:
		return "invalid address"
	case INVALID_TAG:
		return "invalid tag"
	case DATA_TOO_LONG:
		return "data too long"
	case ERROR_READING_FLASH:
		return "error reading flash"
	case SE_ALREADY_INITIALIZED:
		return "secure element already initialized"
	case INVALID_FLASH_PAGE:
		return "invalid flash page"
	case INVALID_BASE64_DATA:
		return "invalid base64 data"
	case ERROR_WRITING_FLASH:
		return "error writing flash"
	case INVALID_JSON:
		return "invalid json data"
	case TOO_FEW_TX:
		return "too few transactions"
	case TOO_MANY_ADDRESSES:
		return "too many addresses"
	case INVALID_COUNT:
		return "invalid cound"
	default:
		return "unknown error"
	}
}
