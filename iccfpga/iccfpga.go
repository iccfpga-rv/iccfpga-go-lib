package iccfpga

import (
	"bytes"
	"io/ioutil"

	. "github.com/iotaledger/iota.go/api"
	"github.com/iotaledger/iota.go/checksum"
	. "github.com/iotaledger/iota.go/consts"
	"github.com/iotaledger/iota.go/signing"
	"github.com/iotaledger/iota.go/trinary"
	. "github.com/iotaledger/iota.go/trinary"
	"github.com/tarm/serial"
)

type ICCFPGAConfig struct {
	Serial        string
	ApiKey        [48]byte
	KeepSeedInRAM bool
	Verbose       bool
	Debug         bool
}

type ICCFPGA struct {
	config ICCFPGAConfig
	flags  map[string]interface{}
	comm   *serial.Config
	port   *serial.Port
}

type ICCFPGALimits struct {
	MaxTransactions uint
	MaxAddresses    uint
}

// Flags holds all allowed flags
type Flags struct {
	KeepSeedInRAM bool
}

type ProgressFunc func(start int, stop int, progress int)

const (
	lenBitstreamBits uint32 = 17536096
)

func min(a int, b int) int {
	if a <= b {
		return a
	} else {
		return b
	}
}

// Init initialize rs232 connection and set flags on fpga
func (iccfpga *ICCFPGA) Init(config ICCFPGAConfig) error {
	iccfpga.config = config
	iccfpga.flags = make(map[string]interface{})
	iccfpga.flags["keepSeedInRAM"] = config.KeepSeedInRAM

	iccfpga.comm = &serial.Config{Name: iccfpga.config.Serial, Baud: 115200}

	var err error
	iccfpga.port, err = serial.OpenPort(iccfpga.comm)
	if err != nil {
		return err
	}

	err = iccfpga.apiSetFlags(iccfpga.flags)
	if err != nil {
		return err
	}
	return nil
}

/*
func (iccfpga *ICCFPGA) GenerateAddressFunc(slot int, index uint64, secLvl SecurityLevel, addChecksum bool) (trinary.Hash, error) {
	trytes, err := iccfpga.apiGenerateAddress(slot, int(index), int(secLvl), 1)
	if err != nil {
		return "", err
	}
	if addChecksum {
		return checksum.AddChecksum(trytes[0], true, 9)
	}
	return trytes[0], err
}
*/
func (iccfpga *ICCFPGA) GenerateAddresses(seed Trytes, index uint64, total uint64, secLvl SecurityLevel, addChecksum bool) ([]Hash, error) {
	addresses := make(Hashes, total)

	number := int(total)
	ofs := int(index)
	for number > 0 {
		chunk := min(number, 100)
		trytes, err := iccfpga.apiGenerateAddress(0, ofs, int(secLvl), chunk)
		if err != nil {
			return nil, err
		}
		for j := 0; j < chunk; j++ {
			if addChecksum {
				addresses[j], err = checksum.AddChecksum(trytes[j], true, 9)
				if err != nil {
					return nil, err
				}
			} else {
				addresses[j] = trytes[j]
			}
		}
		ofs += chunk
		number -= chunk
	}
	return addresses, nil
}

func (iccfpga *ICCFPGA) SignInputs(slot int, inputs []Input, bundleHash Trytes) ([]Trytes, error) {
	signedFrags := []Trytes{}
	for i := range inputs {
		input := &inputs[i]
		var sec SecurityLevel
		if input.Security == 0 {
			sec = SecurityLevelMedium
		} else {
			sec = input.Security
		}

		frags := make([]Trytes, input.Security)
		signedTrytes, err := iccfpga.apiSignBundleHash(slot, uint32(input.KeyIndex), bundleHash, uint32(sec))

		if err != nil {
			return nil, err
		}
		// verify signature
		ok, err := signing.ValidateSignatures(input.Address[:81], signedTrytes, bundleHash)
		if err != nil || !ok {
			return nil, err
		}

		for i := 0; i < int(input.Security); i++ {
			frags[i] = signedTrytes[i]
		}

		signedFrags = append(signedFrags, frags...)
	}
	return signedFrags, nil
}

// PoWFunc PowFunc-Wrapper for PoW
func (iccfpga *ICCFPGA) PoWFunc(trytes Trytes, mwm int, parallelism ...int) (Trytes, error) {
	return iccfpga.apiDoPoW(trytes, mwm)
}

func (iccfpga *ICCFPGA) AttachToTangle(trunk string, branch string, mwm int, trytes []trinary.Trytes) ([]trinary.Trytes, error) {
	trytes, err := iccfpga.apiAttachToTangle(trunk, branch, mwm, trytes)
	if err != nil {
		return nil, err
	}
	return trytes, nil
}

func (iccfpga *ICCFPGA) ReadFlashToBytes(pageOfs int, progress ProgressFunc) ([]byte, error) {
	bytesToRead := int(lenBitstreamBits / 8)
	pagesToRead := bytesToRead / 4096
	if bytesToRead%4096 != 0 {
		pagesToRead += 1
	}
	var core bytes.Buffer
	for page := 0 + pageOfs; page < pagesToRead+pageOfs; page++ {
		decoded, err := iccfpga.apiReadFlashPage(page)
		if err != nil {
			return nil, err
		}

		_, err = core.Write(decoded)
		if err != nil {
			return nil, err
		}
		if progress != nil {
			progress(int(pageOfs), int(pagesToRead+pageOfs), int(page))
		}
	}
	return core.Bytes(), nil
}

func (iccfpga *ICCFPGA) ReadFlashToFile(filename string, pageOfs int, progress ProgressFunc) error {
	data, err := iccfpga.ReadFlashToBytes(pageOfs, progress)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filename, data, 0644)
	return err
}

func (iccfpga *ICCFPGA) WriteFlashFromBytes(pageOfs uint32, data []byte, progress ProgressFunc) error {
	numPages := uint32(len(data) / 4096)

	if len(data)%4096 != 0 {
		numPages += 1
		// simply add 4096 bytes - regardless if they are used or not
		fillBytes := bytes.Repeat([]byte{0xff}, 4096)
		data = append(data, fillBytes...)
	}

	var err error
	for i := pageOfs; i < numPages+pageOfs; i++ {
		err = iccfpga.apiWriteFlashPage(i, data[(i-pageOfs)*4096:(i+1-pageOfs)*4096])
		if err != nil {
			return err
		}
		if progress != nil {
			progress(int(pageOfs), int(numPages+pageOfs), int(i))
		}
	}
	return err
}

func (iccfpga *ICCFPGA) WriteFlashFromFile(filename string, pageOfs uint32, progress ProgressFunc) error {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	return iccfpga.WriteFlashFromBytes(pageOfs, data, progress)
}

func (iccfpga *ICCFPGA) GetVersion() (string, error) {
	return iccfpga.apiGetVersion()
}

func (iccfpga *ICCFPGA) GetLimits() (ICCFPGALimits, error) {
	return iccfpga.apiGetLimits()
}
