package iccfpga

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/iotaledger/iota.go/api"
	"github.com/iotaledger/iota.go/bundle"
	keccak "github.com/iotaledger/iota.go/kerl/sha3"
	"github.com/iotaledger/iota.go/trinary"
	. "github.com/iotaledger/iota.go/trinary"
)

// care for endianess!
func u32ToBytes(value uint32) []byte {
	buf := new(bytes.Buffer)
	err := binary.Write(buf, binary.LittleEndian, value)
	if err != nil {
		return nil
	}
	return buf.Bytes()
}

// care for endianess!
func u64ToBytes(value uint64) []byte {
	buf := new(bytes.Buffer)
	err := binary.Write(buf, binary.LittleEndian, value)
	if err != nil {
		return nil
	}
	return buf.Bytes()
}

func (iccfpga *ICCFPGA) apiCall(cmd map[string]interface{}) (map[string]interface{}, error) {
	//time.Sleep(500 * time.Millisecond)
	bytes, err := json.Marshal(cmd)
	bytes = append(bytes, '\n')

	if iccfpga.config.Debug {
		fmt.Printf("\n------------\n")
		fmt.Printf("-> %s\n", string(bytes))
	}
	written, err := iccfpga.port.Write(bytes)
	if err != nil {
		return nil, errors.New("error writing data")
	}

	if written != len(bytes) {
		return nil, errors.New("not all bytes written")
	}

	reader := bufio.NewReader(iccfpga.port)
	reply, err := reader.ReadBytes('\x0a') // read until \n
	if err != nil {
		return nil, err
	}

	if iccfpga.config.Debug {
		fmt.Printf("<- %s\n", string([]byte(reply)))
	}

	var response map[string]interface{}
	err = json.Unmarshal([]byte(reply), &response)
	if err != nil {
		return nil, err
	}

	if int(response["code"].(float64)) != 200 { // something went wrong ...
		return nil, errors.New(response["error"].(string))
	}

	if iccfpga.config.Verbose {
		fmt.Printf("%s: %dms\n", response["command"], int(response["duration"].(float64)))
	}

	return response, nil
}

// used for http-server
func (iccfpga *ICCFPGA) ApiRawCall(m map[string]interface{}) (map[string]interface{}, error) {
	return iccfpga.apiCall(m)
}

/*
func TestX() {
	key := 123456789
	addressIndex := uint32(987654321)
	bundleHash := "ACRSXEDHISHHDKWJITDCGLEPQQTNWIAYHSZASCUEEGQADTKJONSCFTU9FQ9GLYJLYCSIUXKUWLFQRECID"
	// create api hash
	sha := keccak.New384()
	sha.Write(u32ToBytes(uint32(key)))
	sha.Write(u32ToBytes(addressIndex))
	sha.Write([]byte(string(bundleHash)))
	sha.Write(apiKey[:])
	trits, err := kerl.KerlBytesToTrits(sha.Sum(nil))
	if err != nil {
		return
	}

	apiHash, err := trinary.TritsToTrytes(trits)
	if err != nil {
		return
	}
	fmt.Println(apiHash)
}
*/

func bytesToHex(bytes []byte) string {
	var s string
	for i := 0; i < len(bytes); i++ {
		s = s + fmt.Sprintf("%02x", bytes[i])
	}
	return s
}

func (iccfpga *ICCFPGA) PrepareTransfers(timestamp uint64, slot uint32, transfers []bundle.Transfer, inputs []api.Input, remainderAddress *Hash,
	remainderKeyIndex uint32, trunkTransaction *string, branchTransaction *string, mwm int) (string, []trinary.Trytes, error) {

	cmd := make(map[string]interface{})
	cmd["command"] = "prepareTransfers"
	//	cmd["transfers"] = transfers
	//	cmd["inputs"] = inputs
	cmd["slot"] = slot
	cmd["timestamp"] = timestamp

	sha := keccak.New384()

	sha.Write(u32ToBytes(slot))
	sha.Write(u32ToBytes(uint32(timestamp)))

	var total int64 = 0

	var _transfers []map[string]interface{}
	for _, v := range transfers {
		tmp := make(map[string]interface{})
		tmp["address"] = v.Address[:81]
		tmp["value"] = v.Value
		tmp["message"] = v.Message
		tmp["tag"] = v.Tag
		total += int64(v.Value)
		sha.Write([]byte(v.Address[:81]))
		sha.Write(u64ToBytes(v.Value))
		sha.Write([]byte(v.Tag))
		sha.Write([]byte(v.Message))
		_transfers = append(_transfers, tmp)
	}
	cmd["transfers"] = _transfers

	var _inputs []map[string]interface{}
	for _, v := range inputs {
		tmp := make(map[string]interface{})
		tmp["address"] = v.Address[:81]
		tmp["keyIndex"] = v.KeyIndex
		tmp["balance"] = v.Balance
		total -= int64(v.Balance)
		sha.Write([]byte(v.Address[:81]))
		sha.Write(u32ToBytes(uint32(v.KeyIndex)))
		sha.Write(u64ToBytes(v.Balance))
		_inputs = append(_inputs, tmp)
	}
	cmd["inputs"] = _inputs

	if total < 0 && remainderAddress == nil { // unspent
		return "", nil, errors.New("remainder needed for unspent coins")
	}

	if total > 0 {
		return "", nil, errors.New("insufficient coins")
	}

	if total != 0 {
		_mapRemainder := make(map[string]interface{})
		_mapRemainder["address"] = (*remainderAddress)[:81]
		_mapRemainder["keyIndex"] = remainderKeyIndex
		sha.Write([]byte((*remainderAddress)[:81]))
		sha.Write(u32ToBytes(uint32(remainderKeyIndex)))
		cmd["remainder"] = _mapRemainder
	}

	sha.Write(iccfpga.config.ApiKey[:])
	auth := sha.Sum(nil)
	cmd["auth"] = bytesToHex(auth)

	if trunkTransaction != nil && branchTransaction != nil && mwm >= 9 {
		cmd["trunkTransaction"] = trunkTransaction
		cmd["branchTransaction"] = branchTransaction
		cmd["minWeightMagnitude"] = mwm
	}

	response, err := iccfpga.apiCall(cmd)
	if err != nil {
		return "", nil, err
	}

	responseTrytes := response["trytes"].([]interface{})
	var list []Trytes
	for _, trytes := range responseTrytes {
		slice := Trytes(trytes.(string))
		list = append(list, slice[0:2673])
	}
	return response["bundleHash"].(string), list, nil
}

func (iccfpga *ICCFPGA) BuildAuthSignBundleHash(key uint32, addressIndex uint32, bundleHash string) (string, error) {
	// create api hash
	sha := keccak.New384()
	sha.Write(u32ToBytes(key))
	sha.Write(u32ToBytes(addressIndex))
	sha.Write([]byte(bundleHash))
	sha.Write(iccfpga.config.ApiKey[:])
	auth := sha.Sum(nil)
	/*
		for i := 0; i < 48; i++ {
			fmt.Printf("%2x, ", auth[i])
		}
		fmt.Printf("\n")
	*/
	return bytesToHex(auth), nil
	/*
		trits, err := kerl.KerlBytesToTrits(auth)
		if err != nil {
			return "", err
		}
		authTrytes, err := trinary.TritsToTrytes(trits)
		if err != nil {
			return "", err
		}
		return authTrytes, nil
	*/
}

// SignTransaction signs a transaction
func (iccfpga *ICCFPGA) apiSignBundleHash(slot int, addressIndex uint32, bundleHash Trytes, securityLevel uint32) ([]Trytes, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "signBundleHash"
	cmd["slot"] = slot
	cmd["keyIndex"] = addressIndex
	cmd["bundleHash"] = string(bundleHash)
	cmd["security"] = securityLevel

	auth, err := iccfpga.BuildAuthSignBundleHash(uint32(slot), addressIndex, bundleHash)
	if err != nil {
		return nil, err
	}

	cmd["auth"] = string(auth)

	response, err := iccfpga.apiCall(cmd)
	if err != nil {
		return nil, err
	}

	responseTrytes := response["trytes"].([]interface{})
	var list []Trytes
	for _, trytes := range responseTrytes {
		slice := Trytes(trytes.(string))
		list = append(list, slice[0:2187])
	}
	return list, nil
}

func (iccfpga *ICCFPGA) GenerateRandomSeed(slot int) error {
	return iccfpga.apiGenerateRandomSeed(slot)
}

// GenerateRandomSeed on FPGA
func (iccfpga *ICCFPGA) apiAttachToTangle(trunk string, branch string, mwm int, trytes []trinary.Trytes) ([]trinary.Trytes, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "attachToTangle"
	cmd["trunkTransaction"] = trunk
	cmd["branchTransaction"] = branch
	cmd["minWeightMagnitude"] = mwm
	cmd["trunk"] = trytes
	/*
		var trytesList []string
		for _, v := range trytes {
			trytesList = append(trytesList, string(v))
		}
		cmd["trytes"] = trytesList
	*/
	cmd["trytes"] = trytes

	response, err := iccfpga.apiCall(cmd)
	if err != nil {
		return nil, err
	}

	responseTrytes := response["trytes"].([]interface{})
	var list []Trytes
	for _, trytes := range responseTrytes {
		list = append(list, Trytes(trytes.(string)))
	}
	return list, nil
}

// GenerateRandomSeed on FPGA
func (iccfpga *ICCFPGA) apiGenerateRandomSeed(slot int) error {
	cmd := make(map[string]interface{})
	cmd["command"] = "generateRandomSeed"
	cmd["slot"] = slot
	_, err := iccfpga.apiCall(cmd)
	if err != nil {
		return err
	}

	return nil
}

// GenerateRandomSeed on FPGA
func (iccfpga *ICCFPGA) apiGetVersion() (string, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "version"
	resp, err := iccfpga.apiCall(cmd)

	if err != nil {
		return "", err
	}
	return resp["version"].(string), nil
}

// GenerateRandomSeed on FPGA
func (iccfpga *ICCFPGA) apiGetLimits() (ICCFPGALimits, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "getLimits"
	resp, err := iccfpga.apiCall(cmd)

	if err != nil {
		return ICCFPGALimits{}, err
	}

	var limits ICCFPGALimits
	limits.MaxTransactions = uint(resp["maxTransactions"].(float64))
	limits.MaxAddresses = uint(resp["maxAddresses"].(float64))

	return limits, nil
}

// GenerateAddress generate Address on FPGA
func (iccfpga *ICCFPGA) apiGenerateAddress(slot int, index int, security int, number int) ([]Trytes, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "getAddress"
	cmd["keyIndex"] = index
	cmd["number"] = number
	cmd["security"] = security
	cmd["slot"] = slot
	response, err := iccfpga.apiCall(cmd)
	if err != nil {
		return nil, err
	}

	responseTrytes := response["trytes"].([]interface{})
	var list []Trytes
	for _, trytes := range responseTrytes {
		list = append(list, Trytes(trytes.(string)))
	}
	return list, nil
}

// DoPoW do PoW
func (iccfpga *ICCFPGA) apiDoPoW(trytes Trytes, mwm int) (Trytes, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "doPow"
	cmd["trytes"] = [1]string{trytes}
	cmd["minWeightMagnitude"] = mwm
	response, err := iccfpga.apiCall(cmd)
	if err != nil {
		return "", err
	}

	responseTrytes := response["trytes"].([]interface{})
	return Trytes(responseTrytes[0].(string)), nil
}

func (iccfpga *ICCFPGA) apiSetFlags(flags map[string]interface{}) error {
	cmd := make(map[string]interface{})
	cmd["command"] = "setFlags"
	cmd["flags"] = flags
	_, err := iccfpga.apiCall(cmd)
	return err
}

func (iccfpga *ICCFPGA) apiReadFlashPage(page int) ([]byte, error) {
	cmd := make(map[string]interface{})
	cmd["command"] = "readFlashPage"
	cmd["page"] = page
	response, err := iccfpga.apiCall(cmd)
	if err != nil {
		return nil, err
	}
	decoded, err := base64.StdEncoding.DecodeString(response["data"].(string))
	if len(decoded) != 4096 {
		return nil, errors.New("invalid decoded size")
	}
	return decoded, err
}

func (iccfpga *ICCFPGA) apiWriteFlashPage(page uint32, data []byte) error {
	cmd := make(map[string]interface{})
	cmd["command"] = "writeFlashPage"
	cmd["page"] = page
	if len(data) != 4096 {
		return errors.New("invalid data length")
	}
	encoded := base64.StdEncoding.EncodeToString(data[:])

	cmd["data"] = encoded

	// create api hash
	sha := keccak.New384()
	sha.Write(u32ToBytes(page))
	sha.Write([]byte(data))
	sha.Write(iccfpga.config.ApiKey[:])
	auth := sha.Sum(nil)

	cmd["auth"] = bytesToHex(auth)
	//	fmt.Printf("%s\n", data)
	_, err := iccfpga.apiCall(cmd)
	return err
}

//{"command":"jsonDataTX","timestamp":1500000000,"data":{"hello":"welt"},
//"tag":"999999999999999999999999999","address":"AOTJZ9QOYAF9RUMJXOUOZYX9MBMSVS9NKPBSCODCJSOHHKAVBG99MHHAYOXQLHQIHWNKHM9HQXXQAINAC"
//"minWeightMagnitude":14,"trunkTransaction":"OF9OFDVHVHYPIAVNLLSBZUCQWNKRHRYCIJT9LZEFNAQ9ADSSEDQWUDUSUIKEVSLUJGTDLGGGVZZHZ9999",
//"branchTransaction":"OF9OFDVHVHYPIAVNLLSBZUCQWNKRHRYCIJT9LZEFNAQ9ADSSEDQWUDUSUIKEVSLUJGTDLGGGVZZHZ9999"}
func (iccfpga *ICCFPGA) ApiJsonDataTX(timestamp uint32, address string, tag string,
	trunkTransaction *string, branchTransaction *string, minWeightMagnitude uint32,
	data map[string]interface{}) (Hash, trinary.Trytes, error) {
	cmd := make(map[string]interface{})

	cmd["command"] = "jsonDataTX"
	cmd["timestamp"] = timestamp
	cmd["address"] = address
	cmd["data"] = data
	cmd["tag"] = tag

	if trunkTransaction != nil && branchTransaction != nil {
		cmd["minWeightMagnitude"] = minWeightMagnitude
		cmd["trunkTransaction"] = trunkTransaction
		cmd["branchTransaction"] = branchTransaction
	}

	response, err := iccfpga.apiCall(cmd)

	hash, ok := response["hash"].(string)
	if !ok { // probably not powed, so there is no hash as output
		hash = ""
	}
	trytes, ok := response["trytes"].(string)
	if !ok {
		return "", "", errors.New("invalid response")
	}
	return hash, trytes, err
}
